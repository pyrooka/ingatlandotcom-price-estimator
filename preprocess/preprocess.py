import pandas as pd


def main():
    p = pd.read_json('../output/result.json', lines=True)

    print(p.head())

if __name__ == '__main__':
    main()