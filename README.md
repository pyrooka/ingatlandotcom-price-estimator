# ingatlandotcom-price-estimator

## Crawler
Originally I wanted to use SQLite DB, but the data is too variable so I chose an easier way.
A document oriented DB would be great imho. May in the future.

### Settings
Example with comments:
```
{
    "log_level": "info", // debug, info, warning, error, critical
    "property_type": "house", // house, flat
    "pagination_threads": 5, // Number of threads, which get ID of ads per page.
    "ad_processor_threads": 20, // Number of threads, which crawling datas from an ad.
    "result_file_name": "../output/result.json"
}
```
