# Author: BN
# 2018

import sys
import time
import json
import queue
import logging
import threading
from typing import Union

import requests
from bs4 import BeautifulSoup


LOGGER = None

PAGE_QUEUE = queue.Queue()
DOWNLOAD_QUEUE = queue.Queue()
RESULT_QUEUE = queue.Queue()

SETTINGS = None


def init_settings() -> Union[dict, Exception]:
    '''
    Initialize the settings from the JSON file.
    '''

    try:
        settings = json.load(open('settings.json', 'r'))
        return settings
    except Exception as e:
        return e

def init_logger(log_level: str) -> bool:
    '''
    Initialize the logger.

    Args:
        log_level: debug, info, warning, error, critical. str

    Return:
        Return is the init was success (true) or not (false).
    '''

    global LOGGER

    log_level_dict = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
    }

    if log_level not in log_level_dict:
        return False

    logging.basicConfig(stream=sys.stdout)

    LOGGER = logging.getLogger(__file__)
    LOGGER.setLevel(log_level_dict[log_level])

    return True

def get_page_num(property_type: str) -> int:
    '''
    Get the number of the pages.

    Args:
        property_type: type of the property.

    Returns:
        Number of the pages in int.
    '''

    # Fire a request.
    response = requests.get(SETTINGS['base_url'] + SETTINGS['property_types'][SETTINGS['property_type']], verify=False)

    # Some basic validation.
    if not response or response.status_code is not 200 or not response.text:
        return 0

    # Parse the HTML from the response.
    soup = BeautifulSoup(response.text, 'html.parser')

    # Try to find the pagination div.
    pagination = soup.find('div', attrs={'class': 'pagination__pages'})

    # Some basic validation.
    if not pagination or 'data-total-page' not in pagination.attrs:
        return 0

    # Get the page number attribute.
    pages = pagination.attrs['data-total-page']

    # We need it as an int.
    try:
        pages = int(pages)
    except:
        pages = 0

    return pages

def process_ads(property_type: str) -> None:
    '''
    Process an ad. Extract the parameters and put them to the result queue.

    Args:
        property_type: type of the property in str.
    '''

    LOGGER.info('New thread started with process_ads.')

    while True:
        ad_id = DOWNLOAD_QUEUE.get()

        response = requests.get(SETTINGS['base_url'] + ad_id)

        # Some basic validation.
        if not response or response.status_code is not 200 or not response.text:
            continue

        try:
            soup = BeautifulSoup(response.text, 'html.parser')

            param_price = soup.find('span', attrs={'class': 'essential-parameters__price'})
            param_location = soup.find('h1', attrs={'class': 'essential-parameters__address'})
            param_names = soup.find_all('div', attrs={'class': 'parameter__name'})
            param_values = soup.find_all('div', attrs={'class': 'parameter__value'})
        except Exception as e:
            LOGGER.error('Ad {} skipped. Error: {}'.format(ad_id, e))
            continue

        if len(param_names) != len(param_values):
            LOGGER.error('Ad {} skipped. Error: {}'.format(ad_id, e))
            continue

        param_names = [tag.string for tag in param_names]
        param_values = [tag.string for tag in param_values]

        params = {
            'type': property_type,
            'price': param_price.text.strip(),
            'location': param_location.string,
        }

        for name, value in zip(param_names, param_values):
            params[name] = value

        # Put it to the result queue.
        RESULT_QUEUE.put(params)

        LOGGER.debug('{} ad is processed.'.format(ad_id))

        DOWNLOAD_QUEUE.task_done()

        if DOWNLOAD_QUEUE.empty():
            time.sleep(1)

def process_pages(property_type: str) -> None:
    '''
    Process a page. Extract all the ads' ID then put the IDs to the download queue.

    Args:
        property_type: type of the property in str.
    '''

    LOGGER.info('New thread started with process_pages.')

    while True:
        # Get the next page number from the queue.
        page_num = PAGE_QUEUE.get()

        # Get the page.
        response = requests.get(SETTINGS['base_url'] + SETTINGS['property_types'][SETTINGS['property_type']] + '?page=' + str(page_num))

        # Some basic validation.
        if not response or response.status_code is not 200 or not response.text:
            continue

        try:

            soup = BeautifulSoup(response.text, 'html.parser')

            ads_list = soup.find_all('div', attrs={'class': 'listing js-listing'})

            _ = [DOWNLOAD_QUEUE.put(ad.attrs['data-id']) for ad in ads_list]
        except Exception as e:
            LOGGER.error('Page {} skipped. Error: {}'.format(page_num, e))
            continue

        LOGGER.debug('Page {} is processed.'.format(page_num))

def write_results_to_file(result_file_name: str) -> None:
    '''
    Every 5 sec write all the data from the queue to the output file.

    Args:
        result_file_name: name of the output file.
    '''

    # Running forever.
    while True:

        if not RESULT_QUEUE.empty():

            LOGGER.info('Start writing results to disk.')

            with open(result_file_name, 'a', encoding='utf-8') as file:
                counter = 0
                while not RESULT_QUEUE.empty():
                    counter += 1
                    result = RESULT_QUEUE.get()

                    LOGGER.debug(result)

                    file.write(json.dumps(result, ensure_ascii=False) + '\n')

                    RESULT_QUEUE.task_done()

            LOGGER.info('Written {} results to the output file.'.format(counter))

        # Let the thread relaxing a bit.
        time.sleep(5)

def main():

    global SETTINGS

    # Initialize the settings.
    settings_result = init_settings()

    # If not found or invalid, exit.
    if type(settings_result) is not dict:
        print('Error occured during the settings initialization.')
        print(settings_result)
        sys.exit()

    SETTINGS = settings_result

    # Initalize the logger.
    logger_result = init_logger(SETTINGS['log_level'])

    if not logger_result:
        print('Error during the logger initialization.')
        sys.exit()

    # Get the number of the pages.
    page_num = get_page_num(SETTINGS['property_type'])

    LOGGER.info('Found {} page(s)'.format(page_num))

    if page_num == 0:
        sys.exit()

    # Load all the page numbers to the queue.
    _ = [PAGE_QUEUE.put(i) for i in range(1, page_num)]

    # Start the writer thread, which will write the results to the output file.
    writer_thread = threading.Thread(target=write_results_to_file, args=(SETTINGS['output_file_name'],), daemon=True)
    writer_thread.start()

    pagination_threads = [threading.Thread(target=process_pages, args=(SETTINGS['property_type'],), daemon=True) for _ in range(SETTINGS['pagination_threads'])]

    consumer_threads = [threading.Thread(target=process_ads, args=(SETTINGS['property_type'],), daemon=True) for _ in range(SETTINGS['ad_processor_threads'])]


    for consumer_thread in consumer_threads:
        consumer_thread.start()

    for pagination_thread in pagination_threads:
        pagination_thread.start()

    while True:
        print('Page queue size:', PAGE_QUEUE.qsize())
        print('Download queue size:', DOWNLOAD_QUEUE.qsize())
        print('Result queue size:', RESULT_QUEUE.qsize(), sys.getsizeof(RESULT_QUEUE), 'bytes')
        time.sleep(5)


if __name__ == '__main__':
    main()
